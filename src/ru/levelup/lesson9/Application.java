package ru.levelup.lesson9;

public class Application {

    public static void main(String[] args) throws EmptyStackException {
        Stack<String> stack = new StackArray<>(3);

        stack.push("Element 1");
        stack.push("Element 2");
        stack.push("Element 3");

        String r = stack.pop();
        System.out.println(r);

        stack.push("Element 4");
    }

}
