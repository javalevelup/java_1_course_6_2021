package ru.levelup.lesson9;

// unchecked
public class StackOverflowException extends RuntimeException {

    public StackOverflowException(int stackSize) {
        super("Stack overflow: stack size: " + stackSize);
    }

}
