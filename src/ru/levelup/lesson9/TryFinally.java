package ru.levelup.lesson9;

@SuppressWarnings("ALL")
public class TryFinally {

    public static void main(String[] args) {
        // Что появится на экране?
        System.out.println(returnValue(345));
        System.out.println(returnValue(10));

        noFinally();
    }

    private static int returnValue(int original) {
        try {
            if (original == 10) {
                throw new IllegalArgumentException();
            }
            return 1;
        } catch (Exception exc) {
            return 2;
        } finally {
            return 3;
        }
    }

    public static void noFinally() {
        try {
            System.out.println("try");
            System.exit(0); // выключить JVM
        } catch (Exception exc) {

        } finally {
            System.out.println("finally");
        }
    }

}
