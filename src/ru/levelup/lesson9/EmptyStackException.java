package ru.levelup.lesson9;

// checked
public class EmptyStackException extends Exception {

    public EmptyStackException() {
        super("No elements in the stack");
    }

}
