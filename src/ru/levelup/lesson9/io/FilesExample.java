package ru.levelup.lesson9.io;

import java.io.File;
import java.io.IOException;

public class FilesExample {

    public static void main(String[] args) throws IOException {
        // абсолютный путь
        // относительный путь

        File textFile = new File("text.txt");
        File notExistingFile = new File("not-exist-text.txt");

        // exists()
        boolean isTextFileExist = textFile.exists();
        boolean isSecondFileExist = notExistingFile.exists();

        System.out.println("Is text file exist: " + isTextFileExist);
        System.out.println("Is second file exist: " + isSecondFileExist);

        boolean createResult = notExistingFile.createNewFile();
        System.out.println("Have the file been created: " + createResult);

        long fileSize = textFile.length();
        System.out.println("File size: " + fileSize);

        File srcFolder = new File("src/");

        boolean isSrcFolderExist = srcFolder.exists();
        System.out.println("Is src folder exist: " + isSrcFolderExist);

        File dataFolder = new File("data/folder1"); // project_folder/data/folder1
        boolean isDataFolderExist = dataFolder.exists();
        System.out.println("Is data folder exist: " + isDataFolderExist);

        boolean folderCreateResult = dataFolder.mkdirs();
        System.out.println("Folder create result: " + folderCreateResult);

        System.out.println("Is file: " + textFile.isFile());
        System.out.println("Is folder: " + dataFolder.isDirectory());


        File sourceFolder = new File("src/ru/levelup/");
        for (String filename : sourceFolder.list()) {
            System.out.println(filename);
        }

        // data/folder1/folder2/folder3

    }

}
