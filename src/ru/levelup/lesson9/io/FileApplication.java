package ru.levelup.lesson9.io;

import java.util.ArrayList;
import java.util.Collection;

public class FileApplication {

    public static void main(String[] args) {
        FileService fileService = new FileService();

        Collection<String> strings = new ArrayList<>();
        strings.add("I'm from Saint-Petersburg");
        strings.add("Hello world!");

        fileService.write("text.txt", strings);
        fileService.read("text.txt");
    }

}
