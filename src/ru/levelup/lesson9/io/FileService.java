package ru.levelup.lesson9.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

public class FileService {

    // InputStream
    // OutputStream
    // Reader
    // Writer

    public void read(String filepath) {
//        FileReader fileReader = new FileReader(filepath);
//        BufferedReader reader = new BufferedReader(fileReader);
//        String line = reader.readLine();


//        BufferedReader reader = null;
//        try {
//            reader = new BufferedReader(new FileReader(filepath));
//            // read file
//        } catch (IOException exc) {
//            exc.printStackTrace();
//        } finally {
//            if (reader != null) {
//                try {
//                    reader.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }

        // try-with-resources
        try (BufferedReader reader = new BufferedReader(new FileReader(filepath))) {
            String line;
            while ( (line = reader.readLine()) != null ) {
                System.out.println(line);
            }
        } catch (IOException exc) {
            System.err.println("Couldn't read file: " + exc.getMessage());
        }
    }

    public void write(String filepath, Collection<String> body) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filepath, true))) {

            for (String line : body) {
                writer.write(line);
                writer.newLine();
            }

        } catch (IOException exc) {
            System.err.println("Couldn't write to file " + filepath + ": " + exc.getMessage());
        }
    }

}
