package ru.levelup.lesson9;

// Ограничение:
// 1. Стек имеет фиксированный размер
// 2. Нельзя добавлять элемент в заполненный стек
// 3. Нельзя получить значение из пустого стека

// 1. При добавлении в заполненный стек, нужно сгенерировать исключение
// 2. При попытке получить значение из пустого стека, нужно сгенерировать исключение
public class StackArray<T> implements Stack<T> {

    private final Object[] elements;
    private int insertIndex;

    public StackArray(int capacity) {
        this.elements = new Object[capacity];
    }

    @Override
    public void push(T el) {
        if (insertIndex == elements.length) {
            throw new StackOverflowException(elements.length);
        }

        elements[insertIndex++] = el;
    }

    @Override
    public T pop() throws EmptyStackException {
        if (insertIndex == 0) {
            throw new EmptyStackException();
        }

        // [4, 6, 2, 0, 0, 0]
        // insertIndex = 3

        // insertIndex = insertIndex - 1;
        // return elements[insertIndex];

        // insertIndex = 3 - 1 = 2
        // return elements[2];

        return (T) elements[--insertIndex];
    }

}
