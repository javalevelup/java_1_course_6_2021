package ru.levelup.lesson9;

// LIFO - last in, first out
// add(1), add(4), add(5) - push(1), push(4), push(5)

// [1, 4, 5]

// pop -> возвращает вершину стека
// .pop() -> 5, [1, 4]

// ()()(())
// 4 + 6 * 12
// lisp -> 4 + 6 (a + b) -> + 4 6 (+ (a b))

public interface Stack<T> {

    // Добавить элемент в стек
    void push(T el);

    // Получить элемент из стека
    T pop() throws EmptyStackException;

}
