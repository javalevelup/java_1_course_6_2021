package ru.levelup.lesson7;

import ru.levelup.lesson6.structure.Structure;

public class RouteGraph {

    private String routeName;
    // Отрезки движения машрута
    private Structure lineSegments;

    public RouteGraph(String routeName, Structure lineSegments) {
        this.routeName = routeName;
        this.lineSegments = lineSegments;
    }

    public void addLineSegment(int segmentValue) {
        lineSegments.add(segmentValue);
    }

}
