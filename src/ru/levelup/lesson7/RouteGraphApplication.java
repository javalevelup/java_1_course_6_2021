package ru.levelup.lesson7;

import ru.levelup.lesson6.structure.DynamicArray;
import ru.levelup.lesson6.structure.OneWayList;
import ru.levelup.lesson6.structure.Structure;

public class RouteGraphApplication {

    public static void main(String[] args) {

        Structure<Integer> structure = new OneWayList<>();

        structure.add(54);
        structure.add(30);
        structure.add(12);
        structure.add(67);

        Structure<String> ss = new OneWayList<>();
        ss.add("One");
        ss.add("Two");
        ss.add("Three");

        RouteGraph route = new RouteGraph("M1", structure);


    }

}
