package ru.levelup.lesson7.generic;

public class GenericClass<TYPE> {

    private TYPE value;

    public TYPE getValue() {
        return value;
    }

    public void setValue(TYPE value) {
        this.value = value;
    }

}
