package ru.levelup.lesson7.generic;

public class Application {

    public static void main(String[] args) {

//        GenericClass gc = new GenericClass();
//        gc.setValue("String value");
//        // ..
//        gc.setValue(504);
//        // ..
//        String value = (String) gc.getValue();

        GenericClass<String> gcString = new GenericClass<>();
        gcString.setValue("String value");
        // gcString.setValue(5896);
        String value = gcString.getValue();

        GenericClass<String> gcString2 = new GenericClass<>();
        gcString2.setValue("String value 2");

        GenericClass<Integer> gcInt = new GenericClass<>();
        gcInt.setValue(453);
        Integer intValue = gcInt.getValue();

        GenericClass<Object> gcObject = new GenericClass<>();
        gcObject.setValue(new Object());
        gcObject.setValue("String value 3");
        gcObject.setValue(345d);

        // GenericClass gcRawType = new GenericClass(); // GenericClass<Object> gcObject = new GenericClass<>()

    }

}
