package ru.levelup.lesson7.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CollectionExamples {

    public static void main(String[] args) {

        List<Integer> orders = new ArrayList<>(); // список на основе массива

        orders.add(509);
        orders.add(3);
        orders.add(12);
        orders.add(54);
        orders.add(65);
        orders.add(165);

        // [509, 3, 12, 54, 65, 165, null, null, null, null]
        System.out.println("Количество элементов: " + orders.size());
        System.out.println("Пустой ли список: " + orders.isEmpty());

        // foreach - collections, arrays, any class that implements Iterable<>
        // for (<Collection generic type> <any var> : <collection object>)
        // for (int i = 0; i < orders.size(); i++) {
        //      Integer order = orders.get(i);
        //      ...
        // }
        System.out.println("Содержимое коллекции: ");
        for (Integer order : orders) {
            System.out.println(order); // ConcurrentModificationException (CME)
        }

        System.out.println();
        System.out.println("Содержимое коллекции через Iterator:");
        Iterator<Integer> iterator = orders.iterator();
        while (iterator.hasNext()) {
            Integer order = iterator.next();
            System.out.println(order);
        }

        System.out.println();

        List<Integer> filteredOrders = new ArrayList<>(orders);
        Iterator<Integer> orderIterator = filteredOrders.iterator();
        while (orderIterator.hasNext()) {
            Integer order = orderIterator.next();
            if (order < 50) {
                orderIterator.remove();
            }
        }

        System.out.println("Отфильтрованная коллекция: ");
        for (Integer order : filteredOrders) {
            System.out.println(order);
        }

        System.out.println("Оригинальная коллекция: ");
        for (Integer order : orders) {
            System.out.println(order);
        }

    }

}
