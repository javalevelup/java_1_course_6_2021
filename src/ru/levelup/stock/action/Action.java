package ru.levelup.stock.action;

public interface Action {

    void doAction();

}
