package ru.levelup.stock.action;

import ru.levelup.stock.cache.ProductCache;
import ru.levelup.stock.domain.Product;
import ru.levelup.stock.files.FileService;
import ru.levelup.stock.menu.ConsoleReader;
import ru.levelup.stock.parser.FileLineConverter;
import ru.levelup.stock.parser.ProductFileLineConverter;

import java.io.IOException;
import java.util.List;

public class AddNewProductAction implements Action {

    private static final String PRODUCT_FILE_PATH = "stock/products.txt";

    private final ConsoleReader consoleReader;
    private final FileService fileService;
    private final FileLineConverter<Product> lineConverter;

    public AddNewProductAction() {
        this.consoleReader = new ConsoleReader();
        this.fileService = new FileService();
        this.lineConverter = new ProductFileLineConverter();
    }

    @Override
    public void doAction() {
        // Product: name, vendorCode, weight, price
        String name = consoleReader.readString("Enter a name:");
        String vendorCode = consoleReader.readString("Enter a vendor code:");
        double weight = consoleReader.readDouble("Enter a weight:");
        double price = consoleReader.readDouble("Enter a price:");

        Product product = new Product(name, vendorCode, weight, price);
        boolean isValid = validateProduct(product);

        if (isValid) {
            // write (append) to file
            try {
                String line = lineConverter.toLine(product);
                // fileService.appendToFile(PRODUCT_FILE_PATH, Collections.singletonList(line));
                fileService.appendToFile(PRODUCT_FILE_PATH, List.of(line));
                ProductCache.getInstance().invalidateCache();
            } catch (IOException exc) {
                System.err.println("An error occurred during writing to the file: " + exc.getMessage());
            }
        }
    }

    private boolean validateProduct(Product product) {
        if (product.getName().isBlank()) {
            System.out.println("Name shouldn't be empty");
            return false;
        }

        if (product.getVendorCode().isBlank()) {
            System.out.println("Vendor code shouldn't be empty");
            return false;
        }

        if (Double.isNaN(product.getWeight()) || product.getWeight() <= 0) {
            System.out.println("Weight must be positive number");
            return false;
        }

        if (Double.isNaN(product.getPrice()) || product.getPrice() <= 0) {
            System.out.println("Price must be positive number");
            return false;
        }

        return true;
    }

}
