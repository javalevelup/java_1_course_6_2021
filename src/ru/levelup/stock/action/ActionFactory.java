package ru.levelup.stock.action;

import java.util.HashMap;
import java.util.Map;

// final class - class that cannot be extended
public final class ActionFactory {

    private ActionFactory() {}

    private static final Map<Integer, Action> ACTIONS_MAP = new HashMap<>();

    static {
        ACTIONS_MAP.put(1, new GetProductsAction());
        ACTIONS_MAP.put(2, new AddNewProductAction());
    }

    public static Action findAction(int actionNumber) {
        return ACTIONS_MAP.get(actionNumber);
    }

}
