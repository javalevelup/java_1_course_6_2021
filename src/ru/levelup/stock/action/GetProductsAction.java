package ru.levelup.stock.action;

import ru.levelup.stock.cache.ProductCache;
import ru.levelup.stock.domain.Product;
import ru.levelup.stock.files.FileService;
import ru.levelup.stock.parser.FileLineConverter;
import ru.levelup.stock.parser.ProductFileLineConverter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

// 1. Get list of products
public class GetProductsAction implements Action {

    private static final String PRODUCT_FILE_PATH = "stock/products.txt";

    private final FileService fileService;
    private final FileLineConverter<Product> fileLineConverter;

    public GetProductsAction() {
        this.fileService = new FileService();
        this.fileLineConverter = new ProductFileLineConverter();
    }

    @Override
    public void doAction() {
        // 1. Read file products.txt
        // 2. Parse lines from file
        // 3. Print data

        ProductCache cache = ProductCache.getInstance();
        if (!cache.isFilled()) {
            try {
                Collection<String> lines = fileService.readAllLines(PRODUCT_FILE_PATH);
                Collection<Product> products = new ArrayList<>();

                for (String line : lines) {
                    Product product = fileLineConverter.parseLine(line);
                    products.add(product);
                }

                cache.fillCache(products);

            } catch (IOException exc) {
                System.err.println("Couldn't read file: " + exc.getMessage());
            }
        }

        Collection<Product> products = cache.getProducts();
        for (Product product : products) {
            System.out.println(product);
        }

    }

}
