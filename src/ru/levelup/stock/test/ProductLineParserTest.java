package ru.levelup.stock.test;

import ru.levelup.stock.domain.Product;
import ru.levelup.stock.parser.FileLineConverter;
import ru.levelup.stock.parser.ProductFileLineConverter;

public class ProductLineParserTest {

    public static void main(String[] args) {
        FileLineConverter<Product> productFileLineConverter = new ProductFileLineConverter();
        String line = "Мышь Logitech;ML236456;90.5;406.45";

        Product product = productFileLineConverter.parseLine(line);
        System.out.println(product);
    }

}
