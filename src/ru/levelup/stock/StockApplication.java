package ru.levelup.stock;

import ru.levelup.stock.action.Action;
import ru.levelup.stock.action.ActionFactory;
import ru.levelup.stock.menu.ConsoleMenu;
import ru.levelup.stock.menu.ConsoleReader;

public class StockApplication {

    public static void main(String[] args) {
        ConsoleMenu menu = new ConsoleMenu();
        menu.printMenu();

        int enteredNumber;
        ConsoleReader consoleReader = new ConsoleReader();
        do {
            enteredNumber = consoleReader.readInteger();
            if (enteredNumber > 0) {
                Action action = ActionFactory.findAction(enteredNumber);
                if (action != null) {
                    action.doAction();
                } else {
                    System.out.println("No action for entered number");
                }
            }

        } while (enteredNumber != 0);

    }

}
