package ru.levelup.stock.parser;

/**
 * Общий интерфейс для классов-парсеров строк
 */
public interface FileLineConverter<T> {

    // static final -> UPPER_CASE + _
    String LINE_COLUMN_DELIMETER = ";";

    T parseLine(String line);

    String toLine(T obj);

}
