package ru.levelup.stock.parser;

import ru.levelup.stock.domain.Product;

public class ProductFileLineConverter implements FileLineConverter<Product> {

    private static final int NAME_INDEX = 0;
    private static final int VENDOR_CODE_INDEX = 1;
    private static final int WEIGHT_INDEX = 2;
    private static final int PRICE_INDEX = 3;

    @Override
    public Product parseLine(String line) {
        if (line == null || line.isBlank()) {
            throw new IllegalArgumentException("Line must be not null and not blank");
        }

        String[] split = line.split(LINE_COLUMN_DELIMETER);
        return new Product(
                split[NAME_INDEX],
                split[VENDOR_CODE_INDEX],
                Double.parseDouble(split[WEIGHT_INDEX]),
                Double.parseDouble(split[PRICE_INDEX])
        );
    }

    @Override
    public String toLine(Product product) {
        return String.join(LINE_COLUMN_DELIMETER,
                product.getName(),
                product.getVendorCode(),
                String.valueOf(product.getWeight()),
                String.valueOf(product.getPrice())
        );
    }

}
