package ru.levelup.stock.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleReader {

    // System.in
    // InputStreamReader: transform InputStream to Reader
    private static final BufferedReader CONSOLE_READER = new BufferedReader(new InputStreamReader(System.in));

    public int readInteger() {
        System.out.println("Enter a number:");
        try {
            String line = CONSOLE_READER.readLine();
            return Integer.parseInt(line); // transform java.util.String to Integer
        } catch (IOException exc) {
            System.err.println("IOException was occurred during reading a number");
            throw new RuntimeException(exc);
            // RunExc
            // IllExc
            // IOExc
        } catch (NumberFormatException exc) {
            System.err.println("You entered a wrong string - cannot transform String to Integer.");
            return -1;
        }
    }

    public String readString(String message) {
        try {
            System.out.println(message);
            return CONSOLE_READER.readLine();
        } catch (IOException exc) {
            System.err.println("IOException was occurred during reading a string");
            throw new RuntimeException(exc);
        }
    }

    public double readDouble(String message) {
        try {
            System.out.println(message);
            String line = CONSOLE_READER.readLine();
            return Double.parseDouble(line);
        } catch (IOException exc) {
            System.err.println("IOException was occurred during reading a string");
            throw new RuntimeException(exc);
        } catch (NumberFormatException exc) {
            System.err.println("You entered a wrong string - cannot transform String to Double.");
            return Double.NaN;
        }
    }

}
