package ru.levelup.stock.menu;

public class ConsoleMenu {

    public void printMenu() {
        System.out.println("Actions:");
        System.out.println("1. Get list of products");
        System.out.println("2. Add new product");
        System.out.println("0. Exit program");
    }

}
