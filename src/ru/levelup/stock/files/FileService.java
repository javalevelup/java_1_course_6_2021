package ru.levelup.stock.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class FileService {

    public Collection<String> readAllLines(String filepath) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(filepath))) {
            Collection<String> lines = new ArrayList<>();

            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.isBlank()) {
                    lines.add(line);
                }
            }

            return lines;
        }
    }

    public void appendToFile(String filepath, Collection<String> lines) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filepath, true))) {
            for (String line : lines) {
                writer.newLine();
                writer.write(line);
            }
        }
    }

}
