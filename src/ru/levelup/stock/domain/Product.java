package ru.levelup.stock.domain;

public class Product {

    private String name;
    private String vendorCode;
    private double weight;
    private double price;

    public Product() {}

    public Product(String name, String vendorCode, double weight, double price) {
        this.name = name;
        this.vendorCode = vendorCode;
        this.weight = weight;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", vendorCode='" + vendorCode + '\'' +
                ", weight=" + weight +
                ", price=" + price +
                '}';
    }
}
