package ru.levelup.stock.cache;

import ru.levelup.stock.domain.Product;

import java.util.ArrayList;
import java.util.Collection;

public class ProductCache {

    private Collection<Product> cache;

    private static final ProductCache INSTANCE = new ProductCache();

    private ProductCache() {}

    public static ProductCache getInstance() {
        return INSTANCE;
    }

    // ....
    // products1;
    // cache.fillCache(products1);
    // ...
    // products1.add(new Product());
    public void fillCache(Collection<Product> products) {
        cache = new ArrayList<>(products);
    }

    // clear cache
    public void invalidateCache() {
        cache = null;
    }

    public boolean isFilled() {
        return cache != null;
    }

    public Collection<Product> getProducts() {
        return new ArrayList<>(cache);
    }

}
