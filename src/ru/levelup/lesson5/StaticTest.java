package ru.levelup.lesson5;

@SuppressWarnings("ALL")
public class StaticTest {

    static void printLine(String line) {
        System.out.println(line);
    }

    public static void main(String[] args) {
        StaticTest obj = null;
        obj.printLine("Hello world!");  // -> StaticTest.printLine("Hello world!");
    }

}
