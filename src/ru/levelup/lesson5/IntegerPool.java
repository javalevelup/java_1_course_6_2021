package ru.levelup.lesson5;

@SuppressWarnings("ALL")
public class IntegerPool {

    public static void main(String[] args) {

        // 8 primitive types
        // 8 wrapper-classes

        // boxing - упаковка - primitive -> wrapper-class
        // unboxing - распаковка -> wrapper-class -> primitive

        int intValue = 4;
        Integer integerValue = 4;

        int unboxedIntValue = integerValue.intValue(); // unboxing
        Integer boxedIntegerValue = Integer.valueOf(intValue); // boxing

        // autoboxing/autounboxing
        Integer autoBoxed = intValue;
        int autoUnboxedValue = integerValue; // int autoUnboxedValue = integerValue.intValue();

        // Integer nullValue = null;
        // int unboxed = nullValue; // nullValue.intValue() -> NullPointerException

        Integer i1 = 127; // ref1
        Integer i2 = 127; // ref1
        Integer i3 = 129; // ref2
        Integer i4 = 129; // ref3
        System.out.println(i1 == i2);
        System.out.println(i3 == i4);

    }

}
