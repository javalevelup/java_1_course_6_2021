package ru.levelup.lesson5.equals;

import ru.levelup.lesson5.utils.StringUtils;

import java.util.Objects;

public class Person {

    private String name;
    public int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public static Person createPerson(String name, int age) {
        if (StringUtils.isBlank(name) || age < 0) {
            return null;
        }
        return new Person(name, age);
    }

    @Override
    public boolean equals(Object o) {
        // 1. Проверить, что это один и тот же объект
        if (this == o) return true;
        // 2. Сравнить классы - у аргумента класс совпадает с текущим
        // 2 варианта сравнения типов:
        // 1 вариант: оператор instanceof
        // <instance> instanceof <class name>
        // null instanceof <any class> - always false

        // if (!(o instanceof Person)) return false;

        // 2 вариант: getClass
        if (o == null || this.getClass() != o.getClass()) return false;

        // 3. Привести к типу класса и сравнить поля
        Person other = (Person) o;
        // .. && ((name == other.name) || (name != null && name.equals(other.name))
        return this.age == other.age &&
                Objects.equals(name, other.name);
                // this.name.equals(other.name);
    }

    @Override
    public int hashCode() {
//        int result = 31;
//
//        result += result * 31 + age;
//        result += result * 31 + name.hashCode();
//
//        return result;
        return Objects.hash(age, name);
    }

}
