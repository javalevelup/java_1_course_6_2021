package ru.levelup.lesson5.equals;

public class B extends A {

    int valueB;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        // if (!(o instanceof B)) return false;
        if (o == null || o.getClass() != getClass()) return false;

        B other = (B) o;
        return value == other.value;
    }

}
