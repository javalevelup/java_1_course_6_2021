package ru.levelup.lesson5.equals;

public class A {

    int value;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        //if (!(o instanceof A)) return false;
        if (o == null || getClass() != o.getClass()) return false;

        A other = (A) o;
        return value == other.value;
    }

}
