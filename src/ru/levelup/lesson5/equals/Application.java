package ru.levelup.lesson5.equals;

@SuppressWarnings("ALL")
public class Application {

    public static void main(String[] args) {
        Person person1 = new Person("Ivan", 28);
        Person person2 = Person.createPerson("Ivan", 28);
        Person person3 = person1;

        int intValue1 = 45;
        int intValue2 = 45;

        boolean areEqual = intValue1 == intValue2; // true

        boolean areRefEqual = person1 == person2; // -> false
        System.out.println("Сравнение ссылок: " + areRefEqual);
        boolean areObjEqual = person1.equals(person2); // false
        boolean areObjEqual2 = person2.equals(person1);
        // person1.equals(person3);
        System.out.println("Сравнение двух объектов: " + areObjEqual);
        System.out.println("Хэши двух объектов: " + person1.hashCode() + " - " + person2.hashCode());

        Person person4 = new Person(null, 28);
        System.out.println("Поле name - null: " + person4.equals(person2));



        String s1 = "qwerty";
        String s2 = "qwerty";
        boolean areStringsEqual = s1.equals(s2);

        person1.equals("Ivan");
        person1.equals(null);

    }

}
