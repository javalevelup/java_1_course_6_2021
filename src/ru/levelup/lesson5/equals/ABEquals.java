package ru.levelup.lesson5.equals;

public class ABEquals {

    public static void main(String[] args) {
        A a = new A();
        a.value = 45;

        B b = new B();
        b.value = 45;
        b.valueB = 3;

        System.out.println(a.equals(b)); // b instanceof A -> true
        System.out.println(b.equals(a)); // a instanceof B -> false
    }

}
