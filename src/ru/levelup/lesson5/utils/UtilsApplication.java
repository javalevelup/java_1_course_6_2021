package ru.levelup.lesson5.utils;

public class UtilsApplication {

    public static void main(String[] args) {
        String blankString = "     ";
        String emptyString = "";
        String notEmptyString = "Hello!";

        System.out.println("Blank string: " + StringUtils.isBlank(blankString));
        System.out.println("Null string: " + StringUtils.isBlank(null));
        System.out.println("Empty string: " + StringUtils.isBlank(emptyString));
        System.out.println("Not empty string: " + StringUtils.isBlank(notEmptyString));

        int size = StringUtils.defaultStringSize;
        System.out.println(size);
        StringUtils.defaultStringSize = 15;
        System.out.println(StringUtils.defaultStringSize);

    }

}
