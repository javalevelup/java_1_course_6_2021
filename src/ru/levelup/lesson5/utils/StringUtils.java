package ru.levelup.lesson5.utils;

public class StringUtils {

    public static int defaultStringSize = 16;

    // isBlank (isEmpty) - возвращает true, если строка null или пустая строка или состоит из одних пробелов
    // null
    // ""
    // "      "
    public static boolean isBlank(String string) {
        // String trimString = string.trim();
        // boolean isEmpty = trimString.isEmpty();
        return string == null || string.trim().isEmpty();
    }

}
