package ru.levelup.lesson5.hw;

public class App {

    public static void main(String[] args) {
        Book[] books = {
                new Book("1", "1"),
                new Book("2", "2"),
        };

        // Books[] books = new Book[2];
        // books[0] = ...
        // books[1] = ...

        Reader reader = new Reader();
        reader.takeBooks(books);
    }

}
