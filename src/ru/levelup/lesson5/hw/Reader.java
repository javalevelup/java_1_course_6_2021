package ru.levelup.lesson5.hw;

public class Reader {

    private String name;

    public void takeBook(int books) {
        System.out.println(name + " took " + books + " books");
    }

    public void takeBooks(String[] bookNames) {

    }

    public void takeBooks(Book[] books) {
        String bookNames = "";
        for (int i = 0; i < books.length; i++) {
            bookNames += books[i].getAuthor() + " " + books[i].getName();
        }
        System.out.println(name + " взял книги: " + bookNames);
    }

}
