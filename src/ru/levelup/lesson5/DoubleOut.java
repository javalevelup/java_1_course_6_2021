package ru.levelup.lesson5;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class DoubleOut {

    public static void main(String[] args) {
        NumberFormat format = NumberFormat.getInstance(Locale.forLanguageTag("ru"));
        System.out.println(format.format(5.4));

        Scanner s = new Scanner(System.in);
        //s.useLocale(Locale.ENGLISH);
        double d = s.nextDouble();
        //
    }

}
