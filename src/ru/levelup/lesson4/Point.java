package ru.levelup.lesson4;

// (x, y)
// Access modifiers:
// 1. private - к полям и методам можно обратиться только внутри класса
// 2. default-package (private-package) - не стоит модификатор доступа - доступ внутри пакета
// 3. protected - доступ внутри пакета и в классах-наследниках
// 4. public - доступ отовсюду
public class Point {

    private int x;
    private int y;

    // constructor
    // setter/getter

    public Point() {
        this(2, 2);
    }

    // AllArgsConstructor - конструктор со всеми параметрами
    public Point(int x, int y) {
        System.out.println("Point constructor...");
        this.x = x;
        this.y = y;
    }

    // getter - метод, который возращает значение какого-либо поля
    // public <field_type> get<Field_name>() { return <field> }
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    // setter - метод, который устанавливает значение в поле
    // public void set<Field_name>(<field_type> <field>) { this.<field> = <field>; }
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

}
