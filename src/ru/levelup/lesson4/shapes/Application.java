package ru.levelup.lesson4.shapes;

public class Application {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(6, 9);

        double recPerimeter = rectangle.perimeter();
        System.out.println("Rectangle's perimeter: " + recPerimeter);

        double recSquare = rectangle.square();
        System.out.println("Rectangle's square: " + recSquare);

        Triangle triangle = new Triangle(3, 4, 5);
        double trianglePerimeter = triangle.perimeter();
        System.out.println("Triangle's perimeter: " + trianglePerimeter);

        double triangleSquare = triangle.square();
        System.out.println("Triangle's square: " + triangleSquare);
    }

}
