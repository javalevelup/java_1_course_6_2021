package ru.levelup.lesson4.shapes;

// perimeter()
// square()

public class Shape {

    // size = [5, 3, 5, 3]
    // size = [3, 4, 5]

    protected int[] sizes;

    public Shape(int[] sizes) {
        this.sizes = sizes;
    }

    public double perimeter() {
        double perimeter = 0;
        for (int i = 0; i < sizes.length; i++) {
            perimeter += sizes[i];
        }
        return perimeter;
    }

    public double square() {
        return 0;
    }

}
