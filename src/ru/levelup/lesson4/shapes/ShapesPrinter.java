package ru.levelup.lesson4.shapes;

public class ShapesPrinter {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(5, 6);
        printShapeSquare(rectangle);

        Shape[] shapes = new Shape[3];
        shapes[0] = new Triangle(3, 4, 5);
        shapes[1] = new Triangle(3, 6, 8);
        shapes[2] = new Rectangle(8, 2);

        System.out.println();
        printShapeSquares(shapes);
    }

//    static void printRectangleSquare(Rectangle rectangle) {
//        System.out.println(rectangle.square());
//    }

    static void printShapeSquare(Shape shape) {
        System.out.println(shape.square());
    }

    static void printShapeSquares(Shape[] shapes) {
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].square());
        }
    }

}
