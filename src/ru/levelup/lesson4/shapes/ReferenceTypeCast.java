package ru.levelup.lesson4.shapes;

@SuppressWarnings("ALL")
public class ReferenceTypeCast {

    public static void main(String[] args) {

        Shape s = new Triangle(3, 4, 5);
        // Triangle t = new Triangle(...);
        // Shape s = t;
        double square = s.square(); // 0 vs 6?


        Rectangle rectangle = new Rectangle(5, 6);

        Shape shape = rectangle;                        // неявное расширяющее преобразование
        Object object = shape;
        Object objectRec = rectangle;

        Shape shapeObj = (Shape) object;                // явное сужающее преобразование
        Rectangle rectangleShape = (Rectangle) shape;

        Shape shapeOrigin = new Shape(new int[] {3, 5, 3, 5});
        Rectangle rectangleShapeOrig = (Rectangle) shapeOrigin;

    }

}
