package ru.levelup.lesson4;

public class PointApplication {

    public static void main(String[] args) {

        Point a = new Point(5, 6);
        int aX = a.getX();
        int aY = a.getY();

        System.out.println("A coordinates: (" + aX + ", " + aY + ")");

        a.setX(10);
        a.setY(8);
        System.out.println("A coordinates: (" + a.getX() + ", " + a.getY() + ")");

        System.out.println();
        Point3D point3d = new Point3D(6, 7, 8);

        System.out.println();
        Point3D empty3dPoint = new Point3D();
        System.out.println("Empty point: (" + empty3dPoint.getX() + ", " + empty3dPoint.getY() + ", " + empty3dPoint.getZ() + ")");



    }

}
