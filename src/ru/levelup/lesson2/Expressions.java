package ru.levelup.lesson2;

public class Expressions {

    public static void main(String[] args) {

        int var = 40;
        int second = 30;

        second += var; // second = second + var;

        byte b1 = 45;
        b1 = (byte) (b1 + 50); // 50 - int
        b1 += 50; // b1 = (byte) (b1 + 50)

        boolean condition = b1 < var;
        condition &= second > var; // condition = condition & (second > var);

    }

}
