package ru.levelup.lesson2;

import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) {

        // 1. Ввести значение number с клавиатуры
        // 2. secretNumber генерируется автоматически

        // Для считывания ввода
        Scanner sc = new Scanner(System.in);
        // sc.useLocale(Locale.ENGLISH);
        System.out.println("Введите число: ");
        int number = sc.nextInt(); // вернет число, которое мы ввели
        System.out.println("Вы ввели число " + number);

        // Генерация чисел (ГПСЧ)
        Random r = new Random();
        // [-10; 5) -> r.nextInt(15) - 10; -> [0, 15) - 10 -> [-10, 5)
        // [10; 56) -> r.nextInt(46) + 10; -> [0, 46) + 10 -> [10, 56)
        int secretNumber = r.nextInt(5); // [0, 5)

//        if (number > secretNumber) {
//
//        } else if (number < secretNumber) {
//
//        } else {
//
//        }

        if (number > secretNumber) {
            System.out.println(number + " больше чем " + secretNumber);
        } else if (number < secretNumber) {
            System.out.println(number + " меньше чем " + secretNumber);
        } else {
            System.out.println(number + " равно " + secretNumber);
        }

        // sout
        // if (...) { } else { }
//        if (number > secretNumber) {
//            System.out.println(number + " больше чем " + secretNumber);
//        } else {
//            if (number < secretNumber) {
//                System.out.println(number + " меньше чем " + secretNumber);
//            } else {
//                System.out.println(number + " равно " + secretNumber);
//            }
//        }

    }

}
