package ru.levelup.lesson2;

public class ArrayExample {

    public static void main(String[] args) {

        double price1 = 535.53;
        double price2 = 7685.43;
        double price3 = 694.54;
        double price4 = 8903.45;
        double price5 = 1344.34;

        double avgPrice = (price1 + price2 + price3 + price4 + price5) / 5;

        double[] prices = new double[5];
        // index
        prices[0] = price1;
        prices[1] = price2;
        prices[2] = price3;
        prices[3] = price4;
        prices[4] = price5;

        double sum = 0;
        for (int i = 0; i < prices.length; i++) {
            double el = prices[i];
            System.out.println(el);
            sum += el; // sum = sum + prices[i];
        }
        double avg = sum / prices.length;
        System.out.println(avg);

        // boolean isEqual = 0.4 == (0.3 + 0.1);
        boolean isEqual = Double.compare(0.4, 0.3 + 0.1) == 0; // Double.compare(a, b) == 0
        System.out.println(isEqual);

    }

}
