package ru.levelup.lesson2;

public class Increment {

    // psvm
    public static void main(String[] args) {
        // increment/decrement
        int var = 10;
        // ...
        var = var + 1; // увеличили значение var на 1
        var++; // var = var + 1
        var--;

        // prefix - ++var;
        // suffix (postfix) - var++;
        ++var;

        int a = 10;
        int b = 20;

        int c = a + b++; // a + b++ -> +, increment -> int c = a + b; b = b + 1;
        int d = a + ++b; // a + ++b -> increment, + -> b = b + 1; int d = a + b;

        // int c = a + b;
        // b = b + 1;
        // b = b + 1;
        // int d = a + b;

        // c, d?
        // b?
        System.out.println("c: " + c);
        System.out.println("d: " + d);
        System.out.println("b: " + b);

        int k = 10;
        System.out.println("k++: " + k++);
        System.out.println("++k: " + ++k);

    }

}
