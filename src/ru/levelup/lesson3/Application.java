package ru.levelup.lesson3;

public class Application {

    public static void main(String[] args) {
        double weight = 45.4;

        // объявили переменную с типом Product
        // = new Имя_класса();
        Product phone = new Product(); // 4583adb3
        phone.name = "Phone";          // 4583adb3 -> 4583adb4
        phone.weight = 120.34;
        phone.vendorCode = "AR2345";
        phone.price = 1000;

        Product laptop = new Product();
        laptop.name = "Laptop Lenovo";
        laptop.weight = 210.45;
        laptop.vendorCode = "LL5345";
        laptop.price = 10000;
        // phone/laptop - объекты класса Product

        System.out.println(phone.name + " " + phone.weight + " " + phone.vendorCode);
        System.out.println(laptop.name + " " + laptop.weight + " " + laptop.vendorCode);

        String phonePair = phone.createPairOfVendorCodeAndName();
        System.out.println("Phone pair: " + phonePair);
        System.out.println("Laptop pair: " + laptop.createPairOfVendorCodeAndName());

        System.out.println();
        double phoneDiscountPrice = phone.calculatePriceWithDiscount(30);

        int laptopDiscount = 50;
        double laptopDiscountPrice = laptop.calculatePriceWithDiscount(laptopDiscount);

        System.out.println("Phone discount price: " + phoneDiscountPrice);
        System.out.println("Laptop discount price: " + laptopDiscountPrice);

        laptop.calculatePriceWithDiscount(120);

        System.out.println();
        phone.displayProductInfo();
        laptop.displayProductInfo();
        laptop.displayProductInfo(75);
        laptop.displayProductInfo(35);

        Product mouse = new Product("Logitech", "ML435");
        mouse.displayProductInfo();

//        Product mouse = null;
//        mouse.price = 79.87; // NullPointException (NPE)
//        mouse.displayProductInfo();

    }

}
