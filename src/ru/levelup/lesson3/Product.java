package ru.levelup.lesson3;

// Размер объекта класса:
// 16 байт (заголовки)
// Размер: 16 + 8 (double)  + 4 (name) + 4 (vendorCode) - 32б

// class A { int a; } -> 16 + 4 + 4(добор) = 24
// class B { int a; int b;} -> 16 + 4 + 4 = 24
// class C { int a; int b; int c; } -> 16 + 4 + 4 + 4 + 4(добор) = 32;
// javap -c -v Application.class >> Application.txt
public class Product {

    // Два компоненкта класса:
    // 1. Поля
    // 2. Методы

    // Поле класса (class field)
    String name;
    String vendorCode;
    double weight;
    double price;

    Product() {

    }

    Product(String name, String vendorCode) {
        this.name = name;
        this.vendorCode = vendorCode;
    }

    // Методы
    // <тип_возвращаемого_значения> имяМетода(<тип аргумента1> <имя аргумента1>, <тип агрумента2> <имя аргумента2>) {}
    // Тип возвращаемого значение void - метод не имеет результата

    String createPairOfVendorCodeAndName() {
        String r = vendorCode + ":" + name; // vendorCode:name
        return r;
        // return vendorCode + ":" + name;
    }

    // рассчет скидки
    // 5%
    double calculatePriceWithDiscount(int discountPercent) {
        if (discountPercent < 0 || discountPercent >= 100) {
            System.out.println("Discount is less than 0 or greater than 100");
            return price;
        }

        double discount = discountPercent / 100.0; // int / double -> double
        double priceMultiplier = 1 - discount; // 5% -> 0.05 -> 1 - 0.05 - 0.95 -> price * 0.95;
        return price * priceMultiplier;
    }

    // Сигнатура метода - имя метода + список типов агрументов
    // double calculatePriceWithDiscount(int discountPercent) - calculatePriceWithDiscount(int)
    // void displayProductInfo() - displayProductInfo()
    void displayProductInfo() {
        System.out.println(name + " " + vendorCode + " " + weight + " " + price);
    }

    void displayProductInfo(int discountPercent) {
        double priceWithDiscount = calculatePriceWithDiscount(discountPercent);
        System.out.println(name + " " + vendorCode + " " + weight + " " + price + " ("+ priceWithDiscount + ")");
    }

    // m(int a) {}
    // m(int b) {}

    // double m(int a, double b) - m(int, double)

    // 1. double m(double b, int a) - m(double, int)
    // 2. double m(double a, int b) - m(double, int)
    // 3. double m(int a, int b)    - m(int, int)
    // 4. double m(int b, double a) - m(int, double) - NO
    // 5. int m(int b, double a)    - m(int, double) - NO
    // 6. int m(double a, double b) - m(double, double)
    // 7. double m(double a, double b) - m(double, double)

}
