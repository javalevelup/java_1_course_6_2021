package ru.levelup.lesson8;

import java.util.HashMap;
import java.util.Map;

public class TextGroup {

    public Map<String, Integer> groupWords(String text) {
        // "Text. text, text!"

        // Избавились от символов .,! и перевели все символы в нижний регистр
        String filteredText = text.replace(".", "")
                .replace(",", "")
                .replace("!", "")
                .toLowerCase();

        // String f = text.replace(".", "");
        // f = text.replace(",", "");

        // "text text text"
        String[] words = filteredText.split(" "); // ["text", "text", "text"]

        Map<String, Integer> wordsCount = new HashMap<>();
        for (String word : words) {
            Integer value = wordsCount.get(word);
            if (value == null) {
                // Слово встретили впервые
                wordsCount.put(word, 1);
            } else {
                wordsCount.put(word, value + 1);
            }
        }

        return wordsCount;
    }

}
