package ru.levelup.lesson8.exception;

import java.text.ParseException;
import java.util.Date;

public class Application {

    public static void main(String[] args) {
        Parser parser = new Parser();
        Date date = parser.parseDate("ytfdrhs.12.2021 13:32:54");
        System.out.println(date);

        try {
            parser.parseDateWithoutHandlingException("23542");
        } catch (ParseException exc) {
            exc.printStackTrace();
        }

        try {
            Integer n = null;
            int v = n; // n.intValue() ;
        } catch (NullPointerException | ClassCastException exc) {
            System.out.println("Try to unbox null");
        }
    }

}
