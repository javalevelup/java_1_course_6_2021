package ru.levelup.lesson8.exception;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Parser {

    public Date parseDate(String dateAsString) {
        // date - long - milliseconds from epoch time (linux epoch) - from 01.01.1970 00:00:00 UTC
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        // try-catch-finally
        // try-catch
        // try-finally
        try {
            System.out.println("Before parse");
            Date date = formatter.parse(dateAsString);
            System.out.println("After parse");
            return date;
        } catch (ParseException exc) {
            System.out.println(exc.getMessage());
            exc.printStackTrace();
            return null;
        } finally {
            System.out.println("Finally");
        }
    }

    public Date parseDateWithoutHandlingException(String dateAsString) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return formatter.parse(dateAsString);
    }

}
