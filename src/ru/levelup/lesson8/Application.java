package ru.levelup.lesson8;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Application {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Map<String, Integer> map = new HashMap<>();

        // добавление
        map.put("Milk", 3);
        map.put("Cheese", 1);
        map.put("Tomatoes", 10);
        map.put("Chocolate", 3);
        map.put("Cheese", 2);

        Integer cheeseCount = map.get("Cheese");
        System.out.println("Cheese count: " + cheeseCount);

        int fishCount = map.get("Fish"); // map.get("Fish").intValue();
        System.out.println("Fish count: " + fishCount);

        System.out.println();
        Set<String> keys = map.keySet();
        for (String key : keys) {
            Integer value = map.get(key);
            System.out.println("Key: " + key + ", value: " + value);
        }


        long end = System.currentTimeMillis();
        long dif = end - start;
        //
        System.out.println();
        System.out.println();

        TextGroup textGroup = new TextGroup();

        String text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
        Map<String, Integer> groupedWords = textGroup.groupWords(text);

        Set<String> words = groupedWords.keySet();
        for (String word : words) {
            Integer count = groupedWords.get(word);
            System.out.println("Word: " + word + " - count: " + count);
        }

    }

}
