package ru.levelup.lesson1;

// ru.levelup.lesson1.Primitives - полное имя класса
public class Primitives {

    public static void main(String[] args) {

        int first;
        first = 387;

        int second = 569;

        // Записываем результат умножения в переменную multiply
        int multiply = first * second;
        System.out.println(multiply);

        double total = 4563.32;
        double count = 4; // 4.0

        double price = total / count;
        // Конкатенация строк - соединение строк
        // "Price: " + price -> "Price: " + "1140.83" -> "Price: 1140.83"
        System.out.println("Price: " + price);

        int a = 10;
        int b = 20;
        System.out.println("Sum = " + a + b); // sum = 1020 - "sum = " + 10 + 20 -> "sum = 10" + 20 -> "sum = 1020"
        System.out.println("sum = " + (a + b));
        System.out.println(a + b + " = sum"); // 30 = sum -> 10 + 20 + " = sum" -> 30 + " = sum" -> "30 = sum"

        // sout + Tab/Enter -> System.out.println();
    }

}
