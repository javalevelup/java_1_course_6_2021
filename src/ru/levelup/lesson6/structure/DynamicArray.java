package ru.levelup.lesson6.structure;

// Список на основе массива (динамический массив)
// add() - добавление в конец списка
public class DynamicArray<T> extends AbstractList<T> {

    private Object[] elements;
    private int nextElementIndex;  // индекс, куда мы вставим следующий элемент

    public DynamicArray() {
        this.elements = new Object[3];
        this.size = 0;
        this.nextElementIndex = 0;
    }

    // Garbage collector (GC)
    // добавление элемента в конец списка
    @Override
    public void add(T value) {
        if (elements.length == size) {
            // массив заполнен и нужно его увеличить
            Object[] oldElements = elements;
            elements = new Object[(int)(elements.length * 1.5)];
            System.arraycopy(oldElements, 0, elements, 0, oldElements.length);
            // length = 4 [0..3]
            // el[0], el[1], el[2], el[3]
            // el[1], el[2], el[3], el[4] <<< ArrayIndexOutOfBoundsException
            // arraycopy(elements, 0, elements, 1, elements.length - 1);
        }

        // вставка элемента в массив
        elements[nextElementIndex] = value;
        nextElementIndex++;
        size++;
    }

    // получение элемента списка по индексу
    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Index must be positive and less than size");
        }
        //noinspection unchecked
        return (T) elements[index]; // ArrayIndexOutOfBoundsException
    }

}
