package ru.levelup.lesson6.structure;

public abstract class AbstractList<T2> implements Structure<T2> {

    // количество элементов в списке (размер списка)
    protected int size;

    public abstract void add(T2 value);

    // количество элементов в списке
    @Override
    public int size() {
        return size;
    }

}
