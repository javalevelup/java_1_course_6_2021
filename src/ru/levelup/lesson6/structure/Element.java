package ru.levelup.lesson6.structure;

public class Element<T4> {

    private Element<T4> next;
    private T4 value;

    public Element(T4 value) {
        this.value = value;
        this.next = null;
    }

    public Element<T4> getNext() {
        return next;
    }

    public void setNext(Element<T4> next) {
        this.next = next;
    }

    public T4 getValue() {
        return value;
    }

    public void setValue(T4 value) {
        this.value = value;
    }
}
