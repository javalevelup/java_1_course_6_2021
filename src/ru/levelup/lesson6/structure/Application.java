package ru.levelup.lesson6.structure;

public class Application {

    public static void main(String[] args) {
        // объект списка - первоначально, список пуст
        DynamicArray dynamicArray = new DynamicArray();

        dynamicArray.add(5);    // добавили первый элемент в список
        dynamicArray.add(60);
        dynamicArray.add(23);

        dynamicArray.add(34);

        dynamicArray.add(67);
        dynamicArray.add(90);
        dynamicArray.add(12);

        for (int i = 0; i < dynamicArray.size(); i++) {
            System.out.println(dynamicArray.get(i));
        }

        // AbstractList list = new AbstractList();
        AbstractList list = new DynamicArray();
        Structure s = new DynamicArray();
    }

}
