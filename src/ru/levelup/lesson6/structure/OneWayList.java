package ru.levelup.lesson6.structure;

// Однонаправленный связный список
// Хранит элементы списка в порядке добавления (по-умолчанию)
// Каждый элемент списка состоит из двух вещей: указатель на следующий элемент и значение
public class OneWayList<T3> extends AbstractList<T3> {

    // Начало списка (первый элемент в списке)
    private Element<T3> head;

    @Override
    public void add(T3 value) {
        Element<T3> el = new Element<>(value);
        if (head == null) { // список пуст
            head = el;
        } else { // список не пустой (в нем уже есть значения/элементы)
            // 1. Ищем последний элемент в списке
            Element<T3> current = head;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            // current указывает на последний элемент списка
            current.setNext(el);
        }
        size++;
    }

    @Override
    public T3 get(int index) {
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("Index must be positive and less than size");
        }

        int currentIndex = 0;
        Element<T3> current = head;

        while (current.getNext() != null && currentIndex < index) {
            current = current.getNext();
            currentIndex++;
        }

        return current.getValue();
    }

}
