package ru.levelup.lesson6.structure;

public interface Structure<T1> {

    // public abstract
    void add(T1 value);

    T1 get(int index);

    int size();

}
